import requests, re, os

from bs4 import BeautifulSoup
from bs4.element import Comment
from urllib.parse import urlparse

from tinydb import TinyDB
from textblob import TextBlob

class SiteParser:
    def __init__(self, baseUrl):
        self.internalLinks = [baseUrl]
        self.visited = []
        self.corpus = []
        self.domain = f'http://{urlparse(baseUrl).netloc}'


    def isInternalLink(self, url):
        if self.domain in url:
            return True
        else:
            return False

    def isRelativeLink(self, url):
        if '.' not in url:
            return True
        return False
    
    def handleLink(self, url):
        if self.isRelativeLink(url):
            self.internalLinks.append(f'{self.domain}{url}')
        elif self.isInternalLink(url):
            self.internalLinks.append(url) if url[:4] == 'http' else self.internalLinks.append(f'http://{url}')

    def getContent(self, soup):
        try:
            parents_blacklist=['[document]','html','head',
                            'style','script','body',
                            'div','a','section','tr',
                            'td','label','ul','header',
                            'aside',]
            content=''
            text=soup.find_all(text=True)
        
            for t in text:
                if t.parent.name not in parents_blacklist and len(t) > 10:
                    content=content+t+' '

            self.corpus.append(self.pre_process(content))
        except Exception:
            pass
        
    def pre_process(self, text):
        text=text.lower()
        text=re.sub("</?.*?>"," <> ",text)
        text=re.sub("(\\d|\\W)+"," ",text)
        return text
    
    def parse(self, url):
        if url not in self.visited:
            print(f'visiting {url} ...')
            self.visited.append(url)
            
            # visit page, just move on if exception we have plenty of other pages to work with usually so nbd
            try:
                response = requests.get(url)
            except:
                return

            if response.status_code != 200:
                print(f'returning early because of {response.status_code} code')
                return
            soup = BeautifulSoup(response.content, 'html.parser')

            # collect all links and add them to list of links to visit for this website
            links = soup.find_all('a')
            for link in links:
                if 'href' in link.attrs:
                    self.handleLink(link.attrs['href'])

            # collect content of page and add it to corpus
            self.getContent(soup)

    def run(self):
        i = 0
        while self.internalLinks:
            self.parse(self.internalLinks[i])
            self.internalLinks.pop(i)
        
    def __str__(self):
        return f'Site: {self.domain}, corpus length: {len(self.corpus)}, links parsed: {len(self.visited)}'



db = TinyDB(os.path.join('db.json'))
links = db.table('links')

for page in links.all()[10:11]:
    nouns = []
    noun_phrases = []
    print('page:')
    print(page)
    p = SiteParser(page['external'])
    p.run()

    for text in p.corpus:
        data = TextBlob(text)
        local_nouns = [n for n,t in data.tags if t == 'NN']
        [nouns.append(n) for n in local_nouns if n not in nouns]
        [noun_phrases.append(n) for n in data.noun_phrases if n not in noun_phrases]

    print('nouns')
    print(nouns)
    print('\n')
    print('noun phrases')
    print(noun_phrases)




# tfidf  = TfidfVectorizer(analyzer='word', stop_words='english')
# vec = tfidf.fit_transform(p.corpus)
# terms = tfidf.get_feature_names_out()
# # sum tfidf frequency of each term through documents
# sums = vec.sum(axis=0)

# # connecting term to its sums frequency
# data = []
# for col, term in enumerate(terms):
#     data.append( (term, sums[0,col] ))

# ranking = pd.DataFrame(data, columns=['term','rank'])
# pd.set_option('display.max_rows', 100)

# with open('terms.json', 'w') as fh:
#     fh.write(ranking.sort_values('rank', ascending=False).to_json())
