import os

from bs4 import BeautifulSoup
from bs4.element import Comment
from urllib.parse import urlparse
import requests

from tinydb import TinyDB, Query
db = TinyDB(os.path.join('db.json'))

# clear existing data
db.drop_table('links')
links = db.table('links')

BASE_URL = "https://bcorporation.net"
PAGED_URL = "https://bcorporation.net/directory?search=&industry=&country=United%20States&state=&city=&page="
MAX_PAGES = 100
START_PAGE = 0

def scrape_external_link(link):
    url = f'{BASE_URL}' + link
    response = requests.get(url)
    soup = BeautifulSoup(response.content, 'html.parser')
    return soup.select_one('div.profile__bottom a').attrs['href']

def scrape_internal_links():
    page_no = START_PAGE

    while page_no < MAX_PAGES:
        print(f'visiting page # {page_no} ...')
        url = f'{PAGED_URL}{page_no}'
        response = requests.get(url)
        soup = BeautifulSoup(response.content, 'html.parser')

        # if soup.find_all(string='No results'):
        if soup.find('div', class_='view-empty'):
            break

        pages = soup.find_all('article')
        for page in pages:
            links.insert({'internal': page.attrs['about'], 'external': scrape_external_link(page.attrs['about'])})
        
        page_no = page_no + 1



scrape_internal_links()
