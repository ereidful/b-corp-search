import playwright from 'playwright'

const BASE_URL = `https://bcorporation.net/directory?search=&industry=&country=United%20States&state=&city=`;
const PAGED_URL = `https://bcorporation.net/directory?search=&industry=&country=United%20States&state=&city=&page=`;

async function collectLinks(page, i='none'){
    i == 'none' ? console.log(`collecting links`) : console.log(`collecting links from page: ${i}`)
    const links = await page.$$eval("article a", (cards) => {
        const data = [];
        cards.forEach(card => {
            data.push(card.href)
        })
        return data;
    })
    return links;
}

export default async function scrapeInternalLinks(pagesToScrape=10){
    const browser = await playwright.firefox.launch({headless: true});
    const page = await browser.newPage();
    console.log(`visiting b corp index page: ${BASE_URL}`);
    await page.goto(BASE_URL);

    console.log('scraping page 1')
    const links = await page.$$eval("article a", (cards) => {
        const data = [];
        cards.forEach(card => {
            data.push(card.href)
        })
        return data;
    })

    let i = 1;
    while(i < pagesToScrape || !Number.isInteger(pagesToScrape)){
        await page.goto(`${PAGED_URL}${i++}`);
        try{
            // keep hitting pages until you get to the end, at which point you'll get no result (but a good HTTP response)
            // this will time out after 30 seconds and throw an exception which we catch and break out of the loop
            await page.waitForSelector('article');
            const moreLinks = await collectLinks(page, i);
            links.push(...moreLinks);
        } catch (e){
            break;
        }
    }
    
    await browser.close();

    return links;
}