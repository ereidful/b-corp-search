export default function objectify(arr){
    const obj = {}
    let i = 0;
    arr.forEach(element => {
        obj[i++] = element;
    });

    return obj;
}