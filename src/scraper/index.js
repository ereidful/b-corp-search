import scrapeInternalLinks from './scrapeInternalLinks.js';
import objectify from './objectify.js';
import fs from 'fs';

const internalLinks = await scrapeInternalLinks('all');
const internalLinksObj = objectify(internalLinks);

fs.writeFileSync('./links.json', JSON.stringify(internalLinksObj));